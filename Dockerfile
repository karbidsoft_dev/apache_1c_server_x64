FROM centos:6.8
RUN yum -y install httpd mod_ssl openssl nc && \
yum clean all && \
chkconfig httpd on
COPY rpm /tmp

RUN rpm -ihv /tmp/1C_Enterprise83-common-8.3.7-2027.x86_64.rpm && \
rpm -ihv /tmp/1C_Enterprise83-common-nls-8.3.7-2027.x86_64.rpm && \
rpm -ihv /tmp/1C_Enterprise83-server-8.3.7-2027.x86_64.rpm && \
rpm -ihv /tmp/1C_Enterprise83-server-nls-8.3.7-2027.x86_64.rpm && \
rpm -ihv /tmp/1C_Enterprise83-ws-8.3.7-2027.x86_64.rpm && \
rpm -ihv /tmp/1C_Enterprise83-ws-nls-8.3.7-2027.x86_64.rpm && \
rm -rf /tmp && \
mkdir /tmp && chmod 777 /tmp

#ADD srv1cv83-dev.service /usr/lib/systemd/system/srv1cv83-dev.service
#ADD srv1cv83-dev.service /etc/systemd/system/srv1cv83-dev.service
#RUN ln -s /usr/lib/systemd/system/srv1cv83-dev.service /etc/systemd/system/multi-user.target.wants/srv1cv83-dev.service && \
#chmod 777 /etc/systemd/system/multi-user.target.wants/srv1cv83-dev.service

#RUN systemctl enable srv1cv83.service

EXPOSE 80 443
#CMD ["/usr/sbin/httpd","-DFOREGROUND"]
#ENTRYPOINT ["/usr/sbin/httpd", "-D", "FOREGROUND"]
#CMD ["/usr/sbin/httpd", "-D", "FOREGROUND"]
ENTRYPOINT ["/usr/sbin/apachectl", "-D", "FOREGROUND"]

